package cn.jiangjiesheng.rpc.serialize;

import com.alibaba.fastjson.JSON;

/**
 * 包: cn.jiangjiesheng.rpc.serialize
 * 类名称: JSONDecoder
 * 类描述: 类功能详细描述
 * 作者: jiangjiesheng.cn
 * 创建时间:  2020/4/30 14:10
 */
public class JSONDecoder implements Decoder {
    @Override
    public <T> T decode(byte[] bytes, Class<T> clazz) {
        return JSON.parseObject(bytes, clazz);
    }
}
