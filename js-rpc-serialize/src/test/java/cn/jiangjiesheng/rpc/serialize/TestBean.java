package cn.jiangjiesheng.rpc.serialize;

import lombok.Data;

/**
 * 类作用: 测试Bean
 * 项目名称: js-rpc
 * 包: cn.jiangjiesheng.rpc.serialize
 * 类名称: TestBean
 * 类描述: 类功能详细描述
 * 作者: jiangjiesheng.cn
 * 创建时间:  2020/4/30 14:12
 */
@Data
public class TestBean {
    private String name;
    private int age;
}
