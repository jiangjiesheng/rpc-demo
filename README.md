# RPC
手写简易版RPC框架

运行入口:

cn.jiangjiesheng.rpc.example （js-rpc-example）


以下是运行成功的图：

Server端:

<div align=center style="display:none">
    <img src="http://jiangjiesheng.gitee.io/rpc-demo/images/running-success/running-success-server.jpg" width = "900" height = "0" height-bak = "350" alt="客户端" align=center> 
</div>

```
Connected to the target VM, address: '127.0.0.1:55535', transport: 'socket'
11:01:25 INFO  org.eclipse.jetty.util.log - Logging initialized @978ms to org.eclipse.jetty.util.log.Slf4jLog
11:01:25 INFO  cn.jiangjiesheng.rpc.server.ServiceManager - register service:cn.jiangjiesheng.rpc.example.CalcService add
11:01:25 INFO  cn.jiangjiesheng.rpc.server.ServiceManager - register service:cn.jiangjiesheng.rpc.example.CalcService minus
11:01:25 INFO  org.eclipse.jetty.server.Server - jetty-9.4.28.v20200408; 
                built: 2020-04-08T17:49:39.557Z; git: ab228fde9e55e9164c738d7fa121f8ac5acd51c9; jvm 1.8.0_231-b11
11:01:25 INFO  org.eclipse.jetty.server.handler.ContextHandler - 
                Started o.e.j.s.ServletContextHandler@37654521{/,null,AVAILABLE}
11:01:25 INFO  org.eclipse.jetty.server.AbstractConnector - Started ServerConnector@7dcf94f8{HTTP/1.1, (http/1.1)}{0.0.0.0:3000}
11:01:25 INFO  org.eclipse.jetty.server.Server - Started @1648ms
11:01:48 INFO  cn.jiangjiesheng.rpc.transport.HttpTransportServer - client connect
11:01:48 INFO  cn.jiangjiesheng.rpc.server.RpcServer - get request: 
                Request(service=clazz=cn.jiangjiesheng.rpc.example.CalcService,
                method=add,returnType=int,parameterTypes=[int, int], parameters=[1, 2])
11:01:48 INFO  cn.jiangjiesheng.rpc.server.RpcServer - response client
```

Client端：

<div align=center style="display:none">
    <img src="http://jiangjiesheng.gitee.io/rpc-demo/images/running-success/running-success-client.jpg" width = "900" height = "0" height-bak = "350" alt="客户端" align=center> 
</div>

```
Connected to the target VM, address: '127.0.0.1:57855', transport: 'socket'
11:14:47 INFO  c.jiangjiesheng.rpc.client.RandomTransportSelector 
- connect server: Peer(host=127.0.0.1, port=3000)

3
-1

Disconnected from the target VM, address: '127.0.0.1:57855', transport: 'socket'
Process finished with exit code 0
```

## 项目架构分析

**1.common通用工具模块**

**2.serialize序列化模块**

> ​	序列化和反序列化实现，利用fastjson.JSON实现

**3.transport网络传输模块**

- ```java
  //处理网络请求的handle
  public interface RequestHandler {
      void onRequest(InputStream recive, OutputStream toRespon);
  }
  ```

- ```java
  //RPC server服务定义
  public interface TransportServer {
      //初始化Server服务
      void init(int port, RequestHandler handler);
      //开启Server服务
      void start();
      //关闭Server服务
      void stop();
  }
  ```

- ```java
  //客户端 也是服务消费者
  public interface TransportClient {
      //连接Server服务
      void connect(Peer peer);
      //订阅Server服务  并返回response
      InputStream write(InputStream data);
      //关闭
      void close();
  }
  ```

**4.Server模块**

> ​	主要调用网络传输模块中`HttpTransportServer`，将请求在Handle中实现，并封装在Response中。
>
> ​	服务注册，服务管理，服务发现的实现

**5.Client模块**

> ​	选择一个server端点连接，然后代理反射调用方法。


## 补充说明

**RPC含义**:远程过程直接调用。

**远程调用方式**：RESTful、WebService、HTTP、RPC、基于DB的远程数据调用等

**在RPC中运用的设计模式**：采用消费者模式

​	Server:Provider      Client:Consumer

​	Stub:存根、服务描述

<div align=center>
    <img src="http://jiangjiesheng.gitee.io/rpc-demo/images/image-20200429164542522.png" width = "600" height = "250" alt="客户端" align=center> 
</div>


**需要掌握的技术：**

> Maven、反射、Java的动态代理、序列化(Java对象于二进制数据互转)：fastjson、网络通信：jetty、URLConnection



总结：
<div align=center>
    <img src="http://jiangjiesheng.gitee.io/rpc-demo/images/image-20200508152650728.png" width = "600" height = "350" alt="客户端" align=center> 
</div>

协议制定的类：Request、Response

**实现顺序**

<div align=center>
    <img src="http://jiangjiesheng.gitee.io/rpc-demo/images/image-20200508163233305.png" width = "600" height = "300" alt="客户端" align=center> 
</div>

**类图**

<div align=center>
    <img src="http://jiangjiesheng.gitee.io/rpc-demo/images/image-20200508162952876.png" width = "600" height = "350" alt="客户端" align=center> 
</div>

**难点1：Jetty的嵌入**

Server、ServletContextHandler、ServletHolder

> 代码位置`HttpTransportServer#init`

**难点2:动态代理**

- Proxy.newProxyInstance
- RemoteInvoker implements InvocationHandle