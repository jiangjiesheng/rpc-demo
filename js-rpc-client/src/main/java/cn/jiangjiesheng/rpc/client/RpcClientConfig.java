package cn.jiangjiesheng.rpc.client;

import cn.jiangjiesheng.rpc.Peer;
import cn.jiangjiesheng.rpc.serialize.Decoder;
import cn.jiangjiesheng.rpc.serialize.Encoder;
import cn.jiangjiesheng.rpc.serialize.JSONDecoder;
import cn.jiangjiesheng.rpc.serialize.JSONEncoder;
import cn.jiangjiesheng.rpc.transport.HTTPTransportClient;
import cn.jiangjiesheng.rpc.transport.TransportClient;
import lombok.Data;

import java.util.Arrays;
import java.util.List;

/**
 * 包: cn.jiangjiesheng.rpc.client
 * 类名称: RpcClientConfig
 * 类描述: 类功能详细描述
 * 作者: jiangjiesheng.cn
 * 创建时间:  2020/5/8 13:04
 */
@Data
public class RpcClientConfig {
    private Class<? extends TransportClient> transportClass =
            HTTPTransportClient.class;

    private Class<? extends Encoder> encoderClass = JSONEncoder.class;
    private Class<? extends Decoder> decoderClass = JSONDecoder.class;
    private Class<? extends TransportSelector> selectorClass =
            RandomTransportSelector.class;
    private int connectCount = 1;
    private List<Peer> servers = Arrays.asList(
            new Peer("127.0.0.1", 3000)
    );


}
