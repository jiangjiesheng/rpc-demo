package cn.jiangjiesheng.rpc.example;

import cn.jiangjiesheng.rpc.server.RpcServer;
import cn.jiangjiesheng.rpc.server.RpcServerConfig;

/**
 * 包: cn.jiangjiesheng.rpc.example
 * 类名称: Server
 * 类描述: 类功能详细描述
 * 作者: jiangjiesheng.cn
 * 创建时间:  2020/5/8 14:26
 */
public class Server {
    public static void main(String[] args) {
        RpcServer server = new RpcServer(new RpcServerConfig());
        server.register(CalcService.class, new CalcServiceImpl());
        server.start();
    }
}
