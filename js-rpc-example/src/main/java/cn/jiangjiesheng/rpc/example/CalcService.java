package cn.jiangjiesheng.rpc.example;

/**
 * 包: cn.jiangjiesheng.rpc.example
 * 类名称: CalcServices
 * 类描述: 类功能详细描述
 * 作者: jiangjiesheng.cn
 * 创建时间:  2020/5/8 14:27
 */
public interface CalcService {
    int add(int a, int b);

    int minus(int a, int b);
}
