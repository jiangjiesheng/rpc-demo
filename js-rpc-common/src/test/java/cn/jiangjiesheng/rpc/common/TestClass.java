package cn.jiangjiesheng.rpc.common;

/**
 * 包: cn.jiangjiesheng.rpc.common
 * 类名称: TestClass
 * 类描述: 类功能详细描述
 * 作者: jiangjiesheng.cn
 * 创建时间:  2020/4/29 17:54
 */
public class TestClass {
    public String a() {
        return "a";
    }

    protected String b() {
        return "b";
    }

    private String c() {
        return "c";
    }
}
