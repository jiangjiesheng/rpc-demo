package cn.jiangjiesheng.rpc.server;

import cn.jiangjiesheng.rpc.Request;
import cn.jiangjiesheng.rpc.common.ReflectionUtils;

/**
 * 类作用: 调用具体服务
 * 项目名称: js-rpc
 * 包: cn.jiangjiesheng.rpc.server
 * 类名称: ServiceInvoker
 * 类描述: 类功能详细描述
 * 作者: jiangjiesheng.cn
 * 创建时间:  2020/5/7 18:04
 */
public class ServiceInvoker {
    public Object invoke(ServiceInstance instance, Request request) {
        return ReflectionUtils.invoke(
                instance.getTarget(),
                instance.getMethod(),
                request.getParameters()
        );
    }
}
