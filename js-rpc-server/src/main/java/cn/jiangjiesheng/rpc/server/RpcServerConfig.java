package cn.jiangjiesheng.rpc.server;

import cn.jiangjiesheng.rpc.serialize.Decoder;
import cn.jiangjiesheng.rpc.serialize.Encoder;
import cn.jiangjiesheng.rpc.serialize.JSONDecoder;
import cn.jiangjiesheng.rpc.serialize.JSONEncoder;
import cn.jiangjiesheng.rpc.transport.HttpTransportServer;
import cn.jiangjiesheng.rpc.transport.TransportServer;
import lombok.Data;

/**
 * 类作用: server配置
 * 项目名称: js-rpc
 * 包: cn.jiangjiesheng.rpc.server
 * 类名称: RpcServerConfig
 * 类描述: 类功能详细描述
 * 作者: jiangjiesheng.cn
 * 创建时间:  2020/5/7 13:48
 */
@Data
public class RpcServerConfig {
    private Class<? extends TransportServer> transportClass = HttpTransportServer.class;
    private Class<? extends Encoder> encoderClass = JSONEncoder.class;
    private Class<? extends Decoder> decoderClass = JSONDecoder.class;
    private int port = 3000;


}
