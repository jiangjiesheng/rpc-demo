package cn.jiangjiesheng.rpc.server;

import cn.jiangjiesheng.rpc.Request;
import cn.jiangjiesheng.rpc.ServiceDescriptor;
import cn.jiangjiesheng.rpc.common.ReflectionUtils;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;

import static org.junit.Assert.assertNotNull;

/**
 * 包: cn.jiangjiesheng.rpc.server
 * 类名称: ServiceManagerTest
 * 类描述: 类功能详细描述
 * 作者: jiangjiesheng.cn
 * 创建时间:  2020/5/7 14:31
 */
public class ServiceManagerTest {

    ServiceManager sm;

    @Before
    public void init() {
        sm = new ServiceManager();
        TestInterface bean = new TestClass();
        sm.register(TestInterface.class, bean);
    }

    @Test
    public void register() {
        TestInterface bean = new TestClass();
        sm.register(TestInterface.class, bean);
    }

    @Test
    public void lookup() {
        Method[] methods = ReflectionUtils.getPublicMethods(TestInterface.class);
        ServiceDescriptor sdp = ServiceDescriptor.from(TestInterface.class, methods[0]);
        Request request = new Request();
        request.setService(sdp);
        ServiceInstance lookup = sm.lookup(request);
        assertNotNull(lookup);
    }
}