package cn.jiangjiesheng.rpc.server;

/**
 * 包: cn.jiangjiesheng.rpc.server
 * 类名称: TestInterface
 * 类描述: 类功能详细描述
 * 作者: jiangjiesheng.cn
 * 创建时间:  2020/5/7 14:31
 */
public interface TestInterface {

    void hello();
}
