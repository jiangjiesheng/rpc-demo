package cn.jiangjiesheng.rpc;

import lombok.Data;

/**
 * 类作用: 表示RPC的一个请求
 * 项目名称: js-rpc
 * 包: cn.jiangjiesheng.rpc
 * 类名称: Request
 * 类描述: 类功能详细描述
 * 作者: jiangjiesheng.cn
 * 创建时间:  2020/4/29 17:34
 */
@Data
public class Request {
    private ServiceDescriptor service;
    private Object[] parameters;

}
